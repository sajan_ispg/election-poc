import * as functions from 'firebase-functions';
import * as firebaseAdmin from 'firebase-admin';

firebaseAdmin.initializeApp({
  projectId: "dealden",
  databaseURL: 'https://dealden-default-rtdb.firebaseio.com/',
});

const db = firebaseAdmin.database();
const ref = db.ref('posts');

// // Start writing Firebase Functions
// // https://firebase.google.com/docs/functions/typescript

export const helloWorld = functions.https.onRequest(
  async (request, response) => {
    functions.logger.info('Hello logs!', { structuredData: true });
    const topic = 'highScores';
    let message =
      request.query.message || request.body.message || 'Hello World!';
      const usersRef = ref.child('users');
      usersRef.set({
        message,
        t: `${message}-${Date.now()}`,
      });
    const fireMessage = {
      data: {
        message,
        t: `${message}-${Date.now()}`,
      },
      topic,
    };
    const result = await firebaseAdmin.messaging().send(fireMessage);
    response.send({fireMessage, result});
  }
);

// export const everyFiveMinuteJob = functions.pubsub
//   .schedule('every 5 minutes')
//   .onRun((context) => {
//     console.log('This will be run every 5 minutes!');
//   });
