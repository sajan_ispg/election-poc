import React, { useEffect } from 'react';
import logo from './logo.svg';
import './App.css';

// Import the functions you need from the SDKs you need
// import firebase from 'firebase'
import { initializeApp } from 'firebase/app';
import { getDatabase, ref, onValue } from 'firebase/database';
import { getMessaging, onMessage, getToken } from 'firebase/messaging';
// TODO: Add SDKs for Firebase products that you want to use
// https://firebase.google.com/docs/web/setup#available-libraries

// Your web app's Firebase configuration
// const firebaseConfig = {
//   apiKey: 'AIzaSyA15BmZ0OqH74DI3l3_kQGEXpf62Hpa1JQ',
//   authDomain: 'electiontest-e1475.firebaseapp.com',
//   projectId: 'electiontest-e1475',
//   storageBucket: 'electiontest-e1475.appspot.com',
//   messagingSenderId: '200755463118',
//   appId: '1:200755463118:web:d1fc11a690571d9444e19f',
// };

const firebaseConfig = {
  apiKey: 'AIzaSyDHrF4Z-03T2WG0WFKiCi_oXtQ8SSVrqyo',
  authDomain: 'dealden.firebaseapp.com',
  projectId: 'dealden',
  storageBucket: 'dealden.appspot.com',
  messagingSenderId: '226317171367',
  appId: '1:226317171367:web:1c15e9c06fde8bb7a3b982',
  databaseURL: 'https://dealden-default-rtdb.firebaseio.com/',
};

// Initialize Firebase
const firebaseApp = initializeApp(firebaseConfig);
// const fcm_server_key = 'AAAANLGN-qc:APA91bGoIA0K73c6VIzxCc0RXxfX4g30xZ0Lzgvc7gV34HqRa8udOhxzpHiCDzMzlx-UOhU6jET-aP_7QPLr-gYOsZ3XGKPaEx4qC5csHakZsHB3iDWa1YESW8iNeP6ovhkgRVKK8IPO'
function App() {
  const init = async () => {
    const messaging = getMessaging(firebaseApp);
    // try {
    //   const token = await getToken(messaging, {
    //     vapidKey:
    //       'BErK1hK2Q9JUEevQVFGnYoBEjfgC1ni0qBzUF2AtFvYTHqCtgnFVYD1OgD7pl3GK_H8WGanDTzVcDfjqhJYlJiQ',
    //   });
    //   console.log('Console ~ token', token);

    // } catch (error) {
    //   console.log('Console ~ error', error);
    // }
    const db = getDatabase();
    const starCountRef = ref(db, 'posts/users');
    onValue(starCountRef, (snapshot) => {
      const data = snapshot.val();
      console.log('Console ~ data', data);
    });

    onMessage(messaging, (payload) => {
      console.log('Message received. ', payload);
    });
  };
  useEffect(() => {
    init();
  }, []);
  return (
    <div className="App">
      <header className="App-header">
        <img src={logo} className="App-logo" alt="logo" />
        <p>
          Edit <code>src/App.tsx</code> and save to reload.
        </p>
        <a
          className="App-link"
          href="https://reactjs.org"
          target="_blank"
          rel="noopener noreferrer"
        >
          Learn React
        </a>
      </header>
    </div>
  );
}

export default App;
