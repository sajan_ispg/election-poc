importScripts('https://www.gstatic.com/firebasejs/8.2.2/firebase-app.js');
importScripts('https://www.gstatic.com/firebasejs/8.2.2/firebase-messaging.js');

const vapidKey = 'BErK1hK2Q9JUEevQVFGnYoBEjfgC1ni0qBzUF2AtFvYTHqCtgnFVYD1OgD7pl3GK_H8WGanDTzVcDfjqhJYlJiQ'; //from firebase project settings
const firebaseConfig = {
  apiKey: 'AIzaSyDHrF4Z-03T2WG0WFKiCi_oXtQ8SSVrqyo',
  authDomain: 'dealden.firebaseapp.com',
  projectId: 'dealden',
  storageBucket: 'dealden.appspot.com',
  messagingSenderId: '226317171367',
  appId: '1:226317171367:web:1c15e9c06fde8bb7a3b982',
};

if (!firebase.apps.length) {
  try {
    firebase.initializeApp(firebaseConfig);
  } catch (e) {
    console.log('sw error', e);
  }
}

let messaging = firebase.messaging();
console.log('Console ~ messaging', messaging);

messaging.onBackgroundMessage(function (payload) {
  console.log('firebase Received background message ', JSON.stringify(payload));

  // const notificationTitle = 'Background Message Title';
  // const notificationOptions = {
  //     body: 'Background Message body.',
  //     // icon: '../assets/icon.png'
  // };

  // return self.registration.showNotification(notificationTitle,
  //     notificationOptions);
});
